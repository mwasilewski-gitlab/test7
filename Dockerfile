FROM registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ee:v12.9.2
USER root
RUN sed -i -E 's/#\{user_id\}/\{#\{user_id\}\}/g' /srv/gitlab/app/models/active_session.rb
RUN sed -i -E '/config = raw_config_hash/a return config if config[:cluster]' /srv/gitlab/lib/gitlab/redis/wrapper.rb
USER git:git
